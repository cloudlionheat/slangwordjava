/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author VuongDinh
 */
public class Word {

    private String syntax;
    private String definition;

    public Word() {
        syntax = "";
        definition = "";
    }

    public Word(String syntax, String definition) {
        this.syntax = syntax;
        this.definition = definition;
    }

    public String getDefinition() {
        return definition;
    }

    public String getSyntax() {
        return syntax;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public void setSyntax(String syntax) {
        this.syntax = syntax;
    }

    public boolean isEmpty() {
        return "".equals(syntax);
    }

    public void update(Word wrd) {
        this.syntax = wrd.syntax;
        this.definition = wrd.definition;
    }

    public static Word inputWord() {
        Word word = new Word();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Syntax: ");
        word.syntax = scanner.nextLine();
        System.out.println("Definition: ");
        word.definition = scanner.nextLine();
        return word;
    }

    @Override
    public String toString() {
        return syntax + " : \t" + definition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Word wrd = (Word) o;
        return (this.syntax == null ? wrd.syntax == null : this.syntax.equals(wrd.syntax));
    }

    @Override
    public int hashCode() {
        return Objects.hash(syntax);
    }

}
