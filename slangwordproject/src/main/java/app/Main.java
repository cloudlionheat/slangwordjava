/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author VuongDinh
 */
public class Main {

    public static String menu() {
        System.out.println("\n<=================== Main menu ======================\n");
        System.out.println("Chose your action in this menu:");
        System.out.println("\t1. Search slang word");
        System.out.println("\t2. Your seaching history");
        System.out.println("\t3. On this day word");
        System.out.println("\t4. Play game");
        System.out.println("\t5. Dictionary config");
        System.out.println("\t0. EXIT");
        System.out.println("Your choice: ");
        Scanner scanner = new Scanner(System.in);
        String n = scanner.nextLine();
        if (!"0".equals(n) && !"1".equals(n) && !"2".equals(n) && !"3".equals(n) && !"4".equals(n) && !"5".equals(n)) {
            return "-1";
        }
        return n;
    }

    public static String searchMenu() {
        System.out.println("\n<================== Search slang word ========================\n");
        System.out.println("Search by:");
        System.out.println("\t1. Syntax");
        System.out.println("\t2. Definition");
        System.out.println("\t0. BACK");
        System.out.println("Your choice: ");
        Scanner scanner = new Scanner(System.in);
        String n = scanner.nextLine();
        if (!"0".equals(n) && !"1".equals(n) && !"2".equals(n)) {
            return "-1";
        }
        return n;
    }

    public static void searchBySyntaxMenu(Dictionary dic) {
        System.out.println("\n<==================== Search word by syntax ======================\n");
        System.out.println("To exit searching erase all char and press enter!");
        System.out.println("Type your word: ");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        if ("".equals(str)) {
            return;
        }
        do {
            Word a = dic.findBySyntax(str);
            if (a != null) {
                System.out.println("\nFound your word");
                System.out.println("\t" + a.toString());
            } else {
                System.out.println("\nNot found this word!");
            }
            System.out.println("Press enter to exit or continue search:");
            str = scanner.nextLine();
        } while (!"".equals(str));

    }

    public static void searchByDefinitionMenu(Dictionary dic) {
        System.out.println("\n<=====================Search word by definition=====================\n");
        System.out.println("To exit searching erase all char and press enter!");
        System.out.println("Type your word: ");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        do {
            ArrayList<Word> a = dic.findByDefinition(str);
            if (!a.isEmpty()) {
                System.out.println("\nFound your word");
                Dictionary.showListWord(a);
            } else {
                System.out.println("\nNot found this word!");
            }
            System.out.println("Press enter to exit or continue search:");
            str = scanner.nextLine();
        } while (!"".equals(str));
    }

    public static boolean configMenu(Dictionary dic) throws IOException {
//        Dictionary dic_copy = new Dictionary(dic);
        Scanner scanner = new Scanner(System.in);
        boolean isReset = false, change = false;
        String n;
        int selectWord = -1;
        do {
            System.out.println("\n<================== Dictionary config ========================\n");
            System.out.println("Config dictionary menu:");
            if (selectWord == -1) {
                System.out.println("\tSelected word: nothing");
            } else {
                System.out.println("\tSelected word: \n\t\"" + dic.getListWord().get(selectWord).getSyntax() + "\" " + dic.getListWord().get(selectWord).getDefinition());
            }
            System.out.println("\t1. Add a new word.");
            System.out.println("\t2. Edit a selected word.");
            System.out.println("\t3. Remove a selected word.");
            System.out.println("\t4. Select your word.");
            System.out.println("\t5. Reset to default dictionary./");
            System.out.println("\t6. Save your change.");
            System.out.println("\t0. BACK.");
            System.out.println("Your choice: ");
            n = scanner.nextLine();
            switch (n) {
                case "1" -> {
                    System.out.println("\n<================== Add new word ========================\n");
                    Word a = Word.inputWord();
                    dic.addNewWord(a);
                    System.out.println("\nSuccess add new word!");
                    change = true;
                }
                case "2" -> {

                    if (selectWord == -1) {
                        System.out.println("You must select your word first!");
                    } else {
                        System.out.println("\n<================== Edit word ========================\n");
                        dic.editWord(selectWord, Word.inputWord());
                        System.out.println("\nSuccess edit the word");
                        selectWord = -1;
                        change = true;
                    }
                }
                case "3" -> {

                    if (selectWord == -1) {
                        System.out.println("You must select your word first!");
                    } else {
                        System.out.println("\n<================== Remove word ========================\n");
                        dic.removeWord(selectWord);
                        System.out.println("\nRemove success!");
                        selectWord = -1;
                        change = true;
                    }
                }
                case "4" -> {
                    System.out.println("\n<================== Select word ========================\n");
                    selectWord = selectWordMenu(dic);
                }
                case "5" -> {
                    IOFile fi = new IOFile("slang.txt");
                    dic.setListWord(fi.importSlangWord());
                    System.out.println("Completed reset.");
                    change = true;
                }
                case "6" -> {
                    if (!change) {
                        System.out.println("Nothing is change to save.");
                    } else {
                        IOFile fi = new IOFile("data.txt");
                        fi.exportSlangWord(dic.getListWord());
                        System.out.println("Your change has been save.");
                        isReset = true;
                    }
                }
                case "0" -> {
                    if (!isReset) {
                        System.out.println("Your change did not save yet. Do you want save it?");
                        Scanner scaner = new Scanner(System.in);
                        System.out.println("y/n:");
                        String choice = scaner.nextLine();
                        if ("y".equals(choice)) {
                            IOFile fi = new IOFile("data.txt");
                            fi.exportSlangWord(dic.getListWord());
                            System.out.println("Your change has been save.");
                            isReset = true;
                        }
                    }
                }
                default ->
                    System.out.println("This function is not available. Please try again.");
            }
        } while (!"0".equals(n));
        return isReset;
    }

    public static int selectWordMenu(Dictionary dic) {
        int index = -1;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Your word: ");
            String str = scanner.nextLine();
            if ("".equals(str)) {
                return -1;
            }

            index = dic.getListWord().indexOf(dic.findBySyntax(str));
            if (index == -1) {
                System.out.println("Not found this word. Please try again.");
            }
        } while (index == -1);
        System.out.println("\"" + dic.getListWord().get(index).getSyntax() + "\"" + "is selected.");
        return index;
    }

    public static void main(String[] args) throws IOException {
        String choice;
        boolean isChangeData = false;
        IOFile fi = new IOFile("data.txt");
        Dictionary dic = new Dictionary(fi.importSlangWord());
        System.out.println("Welcome to slang word dictionary".toUpperCase());
        do {

            dic.setListWord(fi.importSlangWord());

            choice = menu();
            switch (choice) {
                case "1" -> {
                    String m;
                    do {
                        m = searchMenu();
                        switch (m) {
                            case "1" ->
                                searchBySyntaxMenu(dic);
                            case "2" ->
                                searchByDefinitionMenu(dic);
                            case "0" ->
                                System.out.println("");
                            default ->
                                System.out.println("\nThis function is not available. Please try again.\n");
                        }
                    } while (!"0".equals(m));
                }
                case "2" -> {
                    System.out.println("\n<================== Searching history ========================\n");
                    System.out.println("Search history:");
                    Dictionary.showListWord(dic.getHistory());
                }
                case "3" -> {
                    System.out.println("\n<================== Random word ========================\n");
                    System.out.println("Today word:");
                    System.out.println("\tSyntax\tDefinition");
                    System.out.println("\t" + dic.randomWord().toString());
                }
                case "4" -> {
                    System.out.println("\n<================== Play game ========================\n");
                    String de;
                    do {
                        Quest.playGame(dic.getListWord());
                        Scanner scanner = new Scanner(System.in);
                        System.out.println("Continue playing? y/n: ");
                        de = scanner.nextLine();
                    } while ("y".equals(de.toLowerCase()));
                }
                case "5" -> {
                    System.out.println("\n<================== Dictionary config ========================\n");
                    isChangeData = configMenu(dic);
                }
                case "0" ->
                    System.out.println("Goodbye see you late <3");
                default ->
                    System.out.println("\nThis function is not available. Please try again.\n");
            }
        } while (!"0".equals(choice));

    }
}
