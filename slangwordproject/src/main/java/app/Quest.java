/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author VuongDinh
 */
public class Quest {

    private Word questWord;
    private boolean type;
    private ArrayList<Word> listAnswer;
    private boolean Correct;

    public Quest() {

        listAnswer = new ArrayList<>();
        questWord = new Word();
        type = true;
        Correct = false;
    }

    public Quest(ArrayList<Word> answer, Word questWord, boolean type) {

        this.listAnswer = answer;
        this.questWord = questWord;
        this.type = type;
        Correct = false;
    }

    public ArrayList<Word> getListAnswer() {
        return listAnswer;
    }

    public Word getQuestWord() {
        return questWord;
    }

    public boolean isCorrect() {
        return Correct;
    }

    public boolean isType() {
        return type;
    }

    public void setCorrect(boolean Correct) {
        this.Correct = Correct;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public void setListAnswer(ArrayList<Word> listAnswer) {
        this.listAnswer = listAnswer;
    }

    public void setQuestWord(Word questWord) {
        this.questWord = questWord;
    }

    public boolean ansQuestions(Word ans) {
        return this.questWord == ans;
    }

    public void showIncorrectAns(int i) {
//        System.out.println("Question " + String.valueOf(i));
        String str = " Giving this ";
        if (type) {
            str += "word \'" + questWord.getSyntax() + "\'. What is the right definition?";
        } else {
            str += "definition \"" + questWord.getDefinition() + "\". What is the right word?";

        }
        if (!Correct) {
            System.out.println("\tQuestion: " + String.valueOf(i) + str);
            if (type) {
                System.out.println("\tCorrect answer: " + questWord.getDefinition());
            } else {
                System.out.println("\tCorrect answer: " + questWord.getSyntax());
            }
        }
    }

    public static int countCorrectAns(ArrayList<Quest> lst) {
        int i = 0;
        for (Quest quest : lst) {
            if (quest.isCorrect()) {
                i++;
            }
        }
        return i;
    }

    public static Quest createQuestion(ArrayList<Word> listWord) {
        Random generator = new Random();
        Quest gam = new Quest();
        int index = generator.nextInt(listWord.size() - 1);
        gam.questWord = listWord.get(index);
        gam.type = generator.nextBoolean();
        for (int i = 0; i < 4; i++) {
            gam.listAnswer.add(listWord.get(index));
//            index = generator.nextInt(listWord.size() - 1);
            while (gam.listAnswer.contains(listWord.get(index))) {
                index = generator.nextInt(listWord.size() - 1);
            }
        }
        Collections.shuffle(gam.listAnswer);
        return gam;
    }

    public static void playGame(ArrayList<Word> listWord) {
        ArrayList<Quest> ds = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < 10; i++) {
            ds.add(createQuestion(listWord));
        }
          System.out.println("\n--------- Game Start ------------");
        for (Quest quest : ds) {
            String choice;
            count++;

            do {
                System.out.println("\nQuestion " + String.valueOf(count) + ": " + quest.toString());
                System.out.println("Your answer:");
                Scanner scanner = new Scanner(System.in);
                choice = scanner.nextLine();
                choice = choice.toLowerCase();

                switch (choice) {
                    case "a" -> {
                        if (quest.listAnswer.get(0) == quest.questWord) {
                            quest.setCorrect(true);
                        }
                    }
                    case "b" -> {
                        if (quest.listAnswer.get(1) == quest.questWord) {
                            quest.setCorrect(true);
                        }
                    }
                    case "c" -> {
                        if (quest.listAnswer.get(2) == quest.questWord) {
                            quest.setCorrect(true);
                        }
                    }
                    case "d" -> {
                        if (quest.listAnswer.get(3) == quest.questWord) {
                            quest.setCorrect(true);
                        }
                    }
                    default ->
                        System.out.println("This answeer is not available. Please try again.");
                }
            } while (!"a".equals(choice) && !"b".equals(choice) && !"c".equals(choice) && !"d".equals(choice));

        }
        System.out.println("\n--------- Result ------------");
        System.out.println("You has finish the game. The correct answer: " + countCorrectAns(ds) + "/10.");
        System.out.println("Your wrong answer: ");

        for (Quest d : ds) {
            System.out.println("");
            d.showIncorrectAns(ds.indexOf(d));
        }

    }

    @Override
    public String toString() {
        String str = "Giving this ";
        if (type) {
            str += "word \'" + questWord.getSyntax() + "\'. What is the right definition?\n";
            str += "A. " + listAnswer.get(0).getDefinition() + "\t\t";
            str += "B. " + listAnswer.get(1).getDefinition() + "\t\t";
            str += "C. " + listAnswer.get(2).getDefinition() + "\t\t";
            str += "D. " + listAnswer.get(3).getDefinition() + "\t";
        } else {
            str += "definition \"" + questWord.getDefinition() + "\". What is the right word?\n";
            str += "A. " + listAnswer.get(0).getSyntax() + "\t\t";
            str += "B. " + listAnswer.get(1).getSyntax() + "\t\t";
            str += "C. " + listAnswer.get(2).getSyntax() + "\t\t";
            str += "D. " + listAnswer.get(3).getSyntax() + "\t\t";
        }
        return str; //To change body of generated methods, choose Tools | Templates.
    }

}
