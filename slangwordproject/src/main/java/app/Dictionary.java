/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.*;

/**
 *
 * @author VuongDinh
 */
public class Dictionary {

    private ArrayList<Word> listWord;
    private ArrayList<Word> history;

    Comparator<Word> compareBySyntax = (Word o1, Word o2)
            -> o1.getSyntax().compareTo(o2.getSyntax());

    public Dictionary() {
        this.listWord = new ArrayList<>();
        this.history = new ArrayList<>();

    }

    public Dictionary(ArrayList<Word> listWord) {
        listWord.sort(compareBySyntax);
        this.listWord = listWord;
        this.history = new ArrayList<>();

    }

    public ArrayList<Word> getListWord() {
        return listWord;
    }

    public ArrayList<Word> getHistory() {
        return history;
    }

    public void setListWord(ArrayList<Word> listWord) {
        this.listWord = new ArrayList<>(listWord);
    }

    public void setHistory(ArrayList<Word> history) {
        this.history = history;
    }

    public Word findBySyntax(String str) {
        HashMap<String, Word> wordMap = new HashMap<>();
        listWord.forEach(word -> {
            wordMap.put(word.getSyntax(), word);
        });
        Word a = wordMap.get(str);
        if (a != null) {
            history.add(a);
        }
        return a;
    }

    public ArrayList<Word> findByDefinition(String str) {
        String[] arrStr = str.toLowerCase().trim().split(" ");
        ArrayList<Word> listSearchWord = new ArrayList<>();

        listWord.forEach(word -> {
            int i = 0;
            String temp = word.getDefinition().toLowerCase();
            for (String searchWord : arrStr) {
                if (temp.contains(searchWord)) {
                    i++;
                }
                if (i >= arrStr.length) {
                    listSearchWord.add(word);
                    history.add(word);
                }
            }
        });

        return listSearchWord;
    }

    public boolean addNewWord(Word newWord) {
        Word wo = findBySyntax(newWord.getSyntax());
        if (wo!=null) {
            System.out.println("This word is has been duplicate. Do you want replace it?");
            Scanner scaner = new Scanner(System.in);
            System.out.println("y/n:");
            String choice = scaner.nextLine();
            if ("y".equals(choice)) {
                int index = listWord.indexOf(wo);
                listWord.get(index).update(newWord);
                return true;
            } else {
                return false;
            }
        }
        listWord.add(newWord);
        listWord.sort(compareBySyntax);
        return true;
    }

    public void editWord(int index, Word newWord) {
        if (!listWord.get(index).getSyntax().equals(newWord.getSyntax()) && listWord.contains(newWord)) {
            System.out.println("This word is duplicate. Do you want replace it?");
            Scanner scaner = new Scanner(System.in);
            System.out.println("y/n:");
            String choice = scaner.nextLine();
            if ("y".equals(choice)) {
                listWord.set(index, newWord);
                System.out.println("This word has been changed successfull.");
            }
        } else {
            listWord.set(index, newWord);
        }
    }

    public void removeWord(int index) {
        System.out.println("Do you want remove this word?");
        Scanner scaner = new Scanner(System.in);
        System.out.println("y/n:");
        String choice = scaner.nextLine();
        if ("y".equals(choice)) {
            listWord.remove(index);
            System.out.println("This word has been remove successful.");
        }
    }

    public Word randomWord() {
        Random generator = new Random();
        return listWord.get(generator.nextInt(listWord.size() - 1));
    }

//    public Word selectedWord() {
//        
//        
//    }
//    public static int checkWord(ArrayList<Word> arr, String x) {
//        
//        return -1;
//    }
    public static void showListWord(ArrayList<Word> list) {
        list.forEach((word) -> {
            System.out.println("\t" + word.toString());
        });
    }

}
