/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author VuongDinh
 */
public class IOFile {

    private String path;

    public IOFile() {
    }

    public IOFile(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ArrayList<Word> importSlangWord() {
        LinkedHashSet<Word> ds = new LinkedHashSet<>();
        try {
            BufferedReader bin = new BufferedReader(
                    new FileReader(path));
            while (true) {
                String str = bin.readLine();
                if (str == null) {
                    break;
                }
                String[] arrStr = str.split("`");
                if (arrStr.length == 2) {
//                    System.out.println(arrStr[1] + "   " + arrStr[0]);
                    Word st = new Word(arrStr[0], arrStr[1]);
                    ds.add(st);
                }
            }
            bin.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>(ds);
    }

    public void exportSlangWord(ArrayList<Word> ds) throws IOException {
        FileWriter fw;
        try {
            fw = new FileWriter(path);
        } catch (IOException exc) {
            System.out.println("Error opening file");
            return;
        }
        for (Word word : ds) {
            fw.write(word.getSyntax() + "`" + word.getDefinition() + "\n");
        }
        fw.close();
    }

//    public static void main(String[] args) throws IOException {
//        IOFile fi = new IOFile("slang.txt");
//        Dictionary dic = fi.importSlangWord();
//        fi.exportSlangWord(dic.getListWord(), "data.txt");
//        System.out.println(dic.getSize());
//        System.out.println(dic.getListWord().get(0).getSyntax());
//    }
}
